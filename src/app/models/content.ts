export interface Content {
    content_title: string,
    content_detail: string,
    content_image: string,
}
export interface ManyContent {
    many_content: Content[]
}
