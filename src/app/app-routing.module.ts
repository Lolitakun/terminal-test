import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from '../app/home/home.component';
import { AboutusComponent } from '../app/aboutus/aboutus.component';
import { ContactComponent } from '../app/contact/contact.component';
import { from } from 'rxjs';
const routes: Routes = [
  {
    path: '',
    children: [
      { 
        path: '', redirectTo: '/home', pathMatch: 'full' 
      },
      { 
        path: 'home', component: HomeComponent 
      },
      {
        path: 'aboutus',component: AboutusComponent
      },
      {
        path: 'contact',component: ContactComponent
      }
    ],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
