import { Component, OnInit } from '@angular/core';
import { ContentHomesService } from '../service/content-homes.service';
import { Content,ManyContent } from '../models/content';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(
    private contentService: ContentHomesService
  ) { }
  content_model: ManyContent;
  content = {
    content_title: 'title',
    content_detail: 'detail',
    content_image: '/assets/logo.png'
  }
  ngOnInit() {
    this.getContent();
  }
  getContent(){
    this.contentService.getContents().subscribe(data => {
      console.log(data)
      this.content_model = data
    });
  }
}
