import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Host } from '../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class ContentHomesService {

  constructor(private http: HttpClient) { }
  getContents() {
    return this.http.get(Host.url + 'content_homes');
  }
  getContentById(id) {
    return this.http.get(Host.url + 'content_homes',id);
  }
  updateContentById(id,data){
    return this.http.patch(Host.url+'content_homes/'+id,data);
  }
  deleteContentById(id){
    return this.http.delete(Host.url+'content_homes/'+id);
  }
}
