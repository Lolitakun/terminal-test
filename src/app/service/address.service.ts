import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Host } from '../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private http: HttpClient) { }
  getAddress() {
    return this.http.get(Host.url + 'address_termials');
  }
  getAddressById(id) {
    return this.http.get(Host.url + 'address_termials',id);
  }
  updateAddressById(id,data){
    return this.http.patch(Host.url+'address_termials/'+id,data);
  }
  deleteAddressById(id){
    return this.http.delete(Host.url+'address_termials/'+id);
  }
}
