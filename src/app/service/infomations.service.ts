import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Host } from '../../environments/environment'
@Injectable({
  providedIn: 'root'
})
export class InfomationsService {

  constructor(private http: HttpClient) { }
  getInfomations() {
    return this.http.get(Host.url + 'infomations');
  }
  getInfomationById(id) {
    return this.http.get(Host.url + 'infomations',id);
  }
  updateInfomationById(id,data){
    return this.http.patch(Host.url+'infomations/'+id,data);
  }
  deleteInfomationById(id){
    return this.http.delete(Host.url+'infomations/'+id);
  }
}
