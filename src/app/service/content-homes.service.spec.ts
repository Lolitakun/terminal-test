import { TestBed } from '@angular/core/testing';

import { ContentHomesService } from './content-homes.service';

describe('ContentHomesService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ContentHomesService = TestBed.get(ContentHomesService);
    expect(service).toBeTruthy();
  });
});
